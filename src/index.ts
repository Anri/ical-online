import { serve } from "@hono/node-server";
import { Hono } from "hono";
/* import * as ical from "node-ical";

ical.async.parseICS("", (_error, data: ical.CalendarResponse) => {
  console.log(data);
}); */

const app = new Hono();
app.get("/", (c) => c.text("Hello Hono!"));

console.log("Listening to http://127.0.0.1:3000");
serve(app);
